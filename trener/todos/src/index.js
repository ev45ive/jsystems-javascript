

Vue.createApp({
  data() {
    return {
      message: 'Hello Vue!',
      id: 1,
      data: null
    }
  },
  methods: {
    loadData() {
      fetch('/todos/' + this.id).then(res => res.json()).then(data => {
        this.data = data
        this.message = 'Loadeed!'
      })
    }
  }
}).mount('#app')


class View extends EventTarget {
  constructor(seletor) {
    super()
    this.el = document.querySelector(seletor)
  }

  update() { throw 'Undefiend method ' }
  render() { throw 'Undefiend method ' }
}


class TodoFormView extends View {

  constructor(selector) {
    super(selector)
    this.el.addEventListener('submit', (event) => {
      event.preventDefault()

      this.formData = new FormData(event.target)
      this.value = Object.fromEntries(this.formData.entries())
      this.dispatchEvent(new Event('todo:created'))
    })
  }
}

class TodosListView extends View {

  data = []


  constructor(selector) {
    super(selector)
    this.el.addEventListener('click', (event) => {
      if (event.target.matches('[type="checkbox"]')) {
        // toggle completed
        this.toggle(event.target.dataset.id, event.target.checked)
      }
      else if (event.target.matches('.close')) {
        // delete
        this.delete(event.target.dataset.id)
      }
    })
  }

  toggle(id, value) {
    console.log('toggle', id);
    fetch('/todos/' + id, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        completed: value
      })
    })
      .then(() => this.update())
  }

  delete(id) {
    console.log('delete', id);

    console.log('toggle', id);
    fetch('/todos/' + id, {
      method: 'DELETE',

    }).then(() => this.update())
  }

  addTodo(data) {
    // this.data.push({ ...data })
    // this.update()
    fetch('/todos', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        "title": data.title,
        "completed": false,
        "userId": 1,
      })
    }).then(res => res.json())
      .then(data => {
        this.data = data
        return this.update()
      })
      .then(() => console.log('Success'))
  }

  update() {
    // fetch('dane.json')
    return fetch('/todos')
      .then(res => res.json())
      .then(data => {
        this.data = data
        this.render()
      })
  }

  render() {
    this.el.innerHTML = `
      ${this.data.map(item => {
      return /* html */`<div class="list-group-item">
          <input type="checkbox" data-id="${item.id}" ${item.completed && 'checked'}/>
          ${item.title}
          <span class="float-end close" data-id="${item.id}">&times;</span>
        </div>`
    }).join('')
      }
    `
  }
}

var todosList = new TodosListView('#todos_list')
var todosForm = new TodoFormView('#todo_form')

todosForm.addEventListener('todo:created', function (event) {
  const formData = event.target.value
  todosList.addTodo(formData)
})


todosList.update()