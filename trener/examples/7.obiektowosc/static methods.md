
```js

function Person(name){
    this.name = name
}
Person.LEGAL_DRIVING_AGE = 18
Person.checkDrivingLicence(person){
    // this == window
    return person.age === Person.LEGAL_DRIVING_AGE 
}

alice = new Person('Alice') 
alice.name 
// 'Alice'

alice.LEGAL_DRIVING_AGE 
// undefined

```

```js
class Person{

  static LEGAL_DRIVING_AGE = 18

  constructor(name){ this.name = name }

  static checkDrivingLicence(person){
      // this == window
      return person.age === Person.LEGAL_DRIVING_AGE 
  }
}

alice = new Person('name')
alice.checkDrivingLicence()
// VM157:1 Uncaught TypeError: alice.checkDrivingLicence is not a function
//     at <anonymous>:1:7
// (anonymous) @ VM157:1

Person.checkDrivingLicence(alice) 
false

```