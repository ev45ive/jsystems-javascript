```js

parent.addEventListener('click',function(event){ ... })
// Clicking Child:

event.target
// <b>child</b>​

event.currentTarget
// <div class=​"list-group" id=​"moje_menu">​…​</div>​ 

event.target.parentElement.parentElement
// <div class=​"list-group-item" data-id=​"2">​…​</div>​

target = event.target
target.matches('.list-group-item')
false

target = event.target
while(!target.matches('.list-group-item')){
    target = target.parentElement
}
target 
// <div class=​"list-group-item" data-id=​"2">​…​</div>​

event.target.closest('.list-group-item')
// <div class=​"list-group-item" data-id=​"2">​…​</div>​

```