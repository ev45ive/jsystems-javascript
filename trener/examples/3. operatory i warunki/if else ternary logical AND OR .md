```js

wynik = 0;

if( x > 3 ) {
    console.log('abc')
    wynik = 1
}else if( x < 2){
    console.log('123')
    wynik = 2
}else{
    console.log('xyz')
}


// =======

if(false)
    console.log('no brackets!'); 
    console.log('outside brackets!'); 
else
    console.log('outside if!'); 

// =======

wynik = warunek == true? 'Prawda' : 'Fałsz';

// ====== 

w = true && 1 && 1 < 2 && 'placki'
// 'placki'
w 
// 'placki'
w1 = true && 1 && 1 < 2 && 'placki'
// 'placki'
w2 = true && 0 && 1 < 2 && 'placki'
// 0
w2 
// 0
w3 =  true && 1 && truthy() 
// VM14851:1 Uncaught ReferenceError: truthy is not defined
//     at <anonymous>:1:1
// (anonymous) @ VM14851:1
w3 =  true && 1 && Math.random() 
// 0.1611090046620678
w3 =  true && (w3 > 1) && Math.random() 
// false

/// =====
w3 =  true || (1 < 2)
// true
w3 = false || (1 < 2)
// true
w3 = false || ('test')
// 'test'

//
// Lazy evaluation 
//

if( false && console.log('not required') ){}
// undefined

false && console.log('not required') 
// false

```